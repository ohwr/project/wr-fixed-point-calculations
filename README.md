# White Rabbit Fixed Point Calculations

This project focuses on performing with high precision the core WR PTP calculations in fixed-point arithmetic. This will ensure uniform input parameters, code and precision across all WR implementations.