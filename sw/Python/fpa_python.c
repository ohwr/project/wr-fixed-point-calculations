#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define SHIFT64 64
#define SHIFT32 32
#define BITS_IN_INT64 (sizeof(int64_t)*8)

#define LOW_OVERFLOW 1

#define HI32(x)	((x) >> 32)
#define LO32(x)	((x) & (uint64_t)0x0ffffffff)

#define MAX 4611686018427387904

typedef int64_t RelativeDifference; // 2^62 scaled value
typedef int64_t TimeInterval; // 2^16 scaled value

static int __getMsbSet(uint64_t value) {
	if ( value==0 )
		return 0; /* value=0 so return bit 0 */
	return BITS_IN_INT64 - __builtin_clzll(value); /* using gcc built-in function */
}

/*
 * Multiplication of two 64 bits numbers by:
 * - splitting op1 and op2 into two 32 bit integers
 * - multiplying every number
 * - shifting and adding every number to get the most accurate 64 bit integer
 * Parameters:
 *  - op1 (first operand): a 2^62 scaled value. op1 must be positive
 *  - op2 (second operand): a 2^X scaled value. op2 must be positive
 *  Return :
 *   (op1 x op2)/2^62. The returned value is a 2^X scaled value
 *
 */
static uint64_t __mulRelativeDifference(RelativeDifference op1, uint64_t op2)
{
	int i;
	int shift;
	uint64_t mask;

	shift = BITS_IN_INT64 - __getMsbSet((op1 < op2) ? op1 : op2); // Calc value of shift with most significant bit

	if(shift > 32)
		shift = 32; // Limit shift to 32 bits

	mask= (1 << (32 - shift)) - 1; // Generate the mask

	uint64_t op1_high = HI32(op1);                                          // splitting into two 32 bit integers
	uint64_t op1_low = LO32(op1);                                           //
	uint64_t op2_high = HI32(op2);                                          //
	uint64_t op2_low = LO32(op2);                                           //

	uint64_t upper = (op1_high * op2_high)<<shift;                          // multiplication
	uint64_t mid1 = op1_high * op2_low;                                     //
	uint64_t mid2 = op2_high * op1_low;                                     //
	uint64_t lower = (op1_low * op2_low)>>LOW_OVERFLOW;                     // potential overflow correction
	uint64_t middle, tmp;

	middle = mid1 + mid2;

	if ((middle < mid1) || (middle < mid2))                                 // Overflow + shifting
	    upper += ((uint64_t)1 << SHIFT32);                                  //
	upper += (middle & ~mask) >> (SHIFT32-shift);                           //
	tmp = lower;                                                            //
	lower += ((middle & mask) << (SHIFT32-LOW_OVERFLOW));                   //

	if (lower < tmp)
		upper += 1;

	return (upper + (lower>>(SHIFT64-shift-LOW_OVERFLOW)))>>(shift-2);      // scale back
}

/*
 * Calculation of delay asymmetry with polynomial expansion
 */
#define TRUE (1==1)

RelativeDifference polyDelayAsymCoeff(RelativeDifference delayCoeff){

	uint64_t term;
	RelativeDifference delayAsymCoeff;
	int negative;
	int sub = 1;

	if((negative=(delayCoeff < 0))==TRUE)                                 // checking whether delayCoeff is negative
		delayCoeff=-delayCoeff;

	delayAsymCoeff = delayCoeff >> 1;                                     // delayCoeff/2

	term  = __mulRelativeDifference(delayCoeff, delayAsymCoeff) >> 1;     // first term of polynomial expansion

	while(term > 2){                                                      // do polynomial expansion until term = 0
		if( !negative){                                               // if delayCoeff is positive
			delayAsymCoeff = sub ?                                //
				delayAsymCoeff - term :                       //
				delayAsymCoeff +term;                         //
			sub = !sub;                                           // Invert for next iteration
		} else {                                                      // if delayCoeff is negative
			delayAsymCoeff += term+1;                             //
		}
		term = __mulRelativeDifference(delayCoeff, term) >> 1;
	}

	return negative ? -delayAsymCoeff : delayAsymCoeff;                   // units of delayAsymCoeff = [ns]*2^62
}

TimeInterval calculateDelayAsymmetryRens (RelativeDifference delayAsymCoeff, TimeInterval scaledMeanDelay  ) {
	int  negDelayAsymCoeff;
	int  negMeanDelay;

	if ( (negMeanDelay=(scaledMeanDelay<0))==TRUE )
		scaledMeanDelay=-scaledMeanDelay;

	if ( (negDelayAsymCoeff=(delayAsymCoeff<0))==TRUE )
		delayAsymCoeff=-delayAsymCoeff;

	TimeInterval delayAsym = __mulRelativeDifference(delayAsymCoeff, scaledMeanDelay);

	return ( negDelayAsymCoeff != negMeanDelay) ? -delayAsym : delayAsym;
}

int main(int argc, char *argv[]){
	FILE * fp;

	char *filename = argv[1];

	int64_t delayCoeff = atoll(argv[2]);
	int64_t end = atoll(argv[3]);
	uint64_t step = atoll(argv[4]);
	TimeInterval scaledMeanDelay = atoll(argv[5]);
	
	RelativeDifference DelayAsymCoeff;
	TimeInterval delayasym;

	fp = fopen (filename, "a+");            // open text file for writhing
	if(fp == NULL){
        	printf("Cannot open filec \n");
        	exit(0);
    	}

	if(end > (int64_t)(MAX)){               // if delayCoeff goes out of it's range delayCoeff = 2^62
		end = (int64_t)(MAX);           //
	}                                       //
		
	while(delayCoeff < end){                                                                   // calculate a/(a+2) until delayCoeff passes the end range
		DelayAsymCoeff = polyDelayAsymCoeff(delayCoeff);                                   //

		if (scaledMeanDelay != 0){                                                         // if meandelayloop.py
			delayasym = calculateDelayAsymmetryRens(DelayAsymCoeff, scaledMeanDelay);  // calc delayasym
			fprintf(fp, "%lld\t%lld\t%lld\n", scaledMeanDelay, delayasym, delayCoeff); // print values to a .txt file
		} else {
			fprintf(fp, "%lld\t%lld\n", delayCoeff, DelayAsymCoeff);                   // print values to a .txt file
		}

		delayCoeff += step;
	}

	fclose(fp);
	return 0;
}
