import subprocess

from mpmath import *
mp.dps = 100

cmd = "fpa_python.c"
subprocess.call(["gcc",cmd]) #For Compiling

finish = mpf(4611686018427387904)	## end value of alpha
begin  = mpf(-4611686018427387903)	## begin value of alpha
start  = mpf(begin)
#stepsize = mpf(1)
stepsize = mpf(7968993439860) #0.000001728 #	    # (3  files step = 5e+05) time: 00:01:05 size: 72   MB
#stepsize = mpf(2656331146620)#0.000000576 #3x above# (4  files step = 1e+06) time: 00:03:00 size: 173  MB|| floating: 4 min 630 MB
#stepsize = mpf(885443715540) #0.000000192 #3x above# (3  files step = 5e+06) time: 00:07:00 size: 430  MB|| floating:
#stepsize = mpf(295147905180) #0.000000064 #3x above# (7  files step = 5e+06) time: 00:20:15 size: 1.4  GB|| floating: 00:40:33 5.7 GB
#stepsize = mpf(59029581036)  #0.0000000128#5x above# (31 files step = 5e+06) time: 01:41:25 size: 7.78 GB|| floating: 03:12:36 28.3 GB 
stepnum = mpf(5e+05)	## number of lines in the files
step = int(mpf(mpf(mpf(finish-begin)/mpf(stepsize))/mpf(stepnum)))+1	## calculation of the number of steps
meandelay = 0
print(step)

for i in range (0, step):		## generation of file names
	str0 = "delayerr"		##
	str1 = "result"			##
	str2 = str(i)			##
	str3 = ".txt"			##
	filename = str1+str2+str3	## result file
	errfile = str0+str2+str3	## error file

	reset_file = open(filename, 'w')## empty result file
	reset_file.write("")		##
	reset_file.close()		##

	end = mpf(mpf(begin) + mpf(mpf(stepsize) * mpf(i+1) * mpf(stepnum)))		## caculation of end value of alpha for the file

	subprocess.call(["./a.out", filename, str(start), str(end), str(stepsize), str(meandelay)])	## compile C code with arguments

	print("donec")

	write_file = open(errfile, 'w')							## open error file
	if(write_file == 0):								##
		print("cant open filep \n")						##
	start = mpf(end)								## calculation of start value of alpha for the next file
	with open(filename, 'r') as f:       						## open result file
		for line in f:
			alpha2, delayasym = line.split("\t")					## allocate the values in the file
			alpha2 = int(mpf(alpha2))						##
			delayasym = int(mpf(delayasym))						##

			real = mpf(mpf(mpf(alpha2)/mpf(mpf(alpha2)+mpf(2**63)))*mpf(2**62))	## calculate alpha/(alpha+2) with 100 decimal digit precision
			delayerr = int(mpf(mpf(real) - mpf(delayasym)))				## calculate the error
			alpha2 = mpf(mpf(alpha2)/mpf(2**62))						##

			write_file.write("%s\t" % alpha2)					## fill error file
			#write_file.write("%s\t" % real)					##
			write_file.write("%s\n" % delayerr)					##
	print(i)
