#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "calc_fun.h"

//#define ALPHALOOP						// uncomment to use python test code alphaloop.py
//#define MEANDELAYLOOP					// uncomment to use python tehs code meandelayloop.py
//#define SIZE_OPTIMISATION				// uncomment to optimise for the size of the code

#define ALPHA_SHIFT 1
#define MEANDELAY_SHIFT 20
#define ALPHA_MASK	(0xffffffff)
#define MEANDELAY_MASK	(0x00000fff)

#define MAX 4611686018427387904
#define SHIFT64 64
#define SHIFT32 (SHIFT64 >> 1)

#define ZERO 0

#define HI(x)	((x) >> 32)
#define LO(x)	((x) & 0x00000000ffffffff)

/*
 * multiplication of 2 64 bit numbers by:
 * - splitting mul1 and mul2 into two 32 bit integers
 * - multiplying every number
 * - shifting and adding every number to get the most accurate 64 bit integer
 */
uint64_t mul64(uint64_t mul1, uint64_t mul2, int shift, uint32_t mask, int j)
{
	uint64_t mul1_high = HI(mul1);								// splitting into two 32 bit integers
	uint64_t mul1_low = LO(mul1);								//
	uint64_t mul2_high = HI(mul2);								//
	uint64_t mul2_low = LO(mul2);								//

	uint64_t upper = (mul1_high * mul2_high)<<(shift);			// multiplication
	uint64_t mid1 = mul1_high * mul2_low;						//
	uint64_t mid2 = mul2_high * mul1_low;						//
	uint64_t lower = mul1_low * mul2_low;						//
	uint64_t middle, tmp;

																// Overflow + shifting
	middle = mid1 + mid2;										//
	if ((middle < mid1) || (middle < mid2))						//
	    upper += ((uint64_t)1 << SHIFT32);						//
	upper += (middle & ~(uint64_t)(mask)) >> (SHIFT32-shift);	//
	tmp = lower;												//
	lower += (middle & mask) << SHIFT32;						//

	#ifndef SIZE_OPTIMISATION									// if size optimisation is not needed
		if(j == 1 && (((middle >> 30) & 0x01) == 1))			//
		upper += 1;												//
	#endif														//

	if (lower < tmp)
		upper += 1;

	return (upper + (lower>>(SHIFT64-shift)));
}

/*
 * Calculation of delay asymmetry with polynomial expansion
 */
int64_t poli_delayasym(int64_t alpha, uint64_t meandelay){ // alpha = alpha*2^62 = delaycoefficient, meandelay is scaled to [ns]*2^16

	uint64_t ualpha;
	uint64_t term, delayasym;

	int j = 0;
	int sub = 1;

	if(alpha < 0){							// checking whether alpha is:
		ualpha = (uint64_t)(-alpha);		// negative
		j = 1;								//
	} else {								// or positive
		ualpha = (uint64_t)(alpha);			//
		j = 0;								//
	}

	delayasym = (ualpha >> 1);				// alpha/2

	term  = mul64(ualpha, ualpha, ALPHA_SHIFT, ALPHA_MASK, j) >> 1;	// first term of polynomial expansion

	while(term > 2){												// do polynomial expansion until term = 0
		if(j == 0){													// if alpha is positive
			if(sub)													//
				delayasym -= term;									//
			else													//
				delayasym += term;									//
			sub = !sub;												//
		} else {													// if alpha is negative
			delayasym += term;										//
			#ifdef SIZE_OPTIMISATION								//
				delayasym += 1;										//
			#endif
		}
		term = mul64(ualpha, term, ALPHA_SHIFT, ALPHA_MASK, j);
	}

	#ifndef ALPHALOOP				// calculation of the delayasymmetry (not used whith alphaloop.py)
		delayasym = mul64(meandelay, delayasym, MEANDELAY_SHIFT, MEANDELAY_MASK, ZERO);
	#endif

	if(j == 1){
		delayasym = -(int64_t)(delayasym);
	}

	return delayasym;		// units of delayasym = [ns]*2^16*2^18
}


#ifdef ALPHALOOP
int main(int argc, char *argv[]){
	FILE * fp;

	char *filename = argv[1];

	int64_t start = atoll(argv[2]);
	int64_t end = atoll(argv[3]);
	int64_t stepsize = atoll(argv[4]);

	uint64_t step = stepsize;
	int64_t alpha = start;
	int64_t delayasym;

	int j = 0;

	fp = fopen (filename, "w");				// empty text file
    if (fp == NULL)
    {
        printf("Cannot open filec1 \n");
        exit(0);
    }
	fprintf(fp, NULL);
	fclose(fp);

	fp = fopen (filename, "a+");			// open text file for writhing
	if(fp == NULL)
    {
        printf("Cannot open filec2 \n");
        exit(0);
    }


	if(end > (int64_t)(MAX)){				// if alpha goes out of it's range alpha = 2^62
		end = (int64_t)(MAX);				//
	}										//

	while(alpha < end){										// calculate a/(a+2) until alpha passes the end range
			delayasym = poli_delayasym(alpha, ZERO);		//
			fprintf(fp, "%lld\t%lld\n", alpha, delayasym);	// print alpha and a/(a+2) in a file
			alpha += step;									//
	}														//
	fclose(fp);
	printf("donec");
	return 0;
}
#endif

#ifdef MEANDELAYLOOP
int main(int argc, char *argv[]){
	FILE * fp;

	char *filename = argv[1];

	int64_t start = atoll(argv[2]);
	int64_t end = atoll(argv[3]);
	int64_t stepsize = atoll(argv[4]);
	uint64_t meandelay = atoll(argv[5]);

	uint64_t step = stepsize;
	int64_t alpha = start;
	uint64_t temp;
	int64_t delayasym;
	int j = 0;

	fp = fopen (filename, "a+");			// open text file to continue writing
	if(fp == NULL)
    	{
        	printf("Cannot open filec \n");
        	exit(0);
    	}


	while(alpha < end){													// loop through the entire range of alpha									//
		delayasym = poli_delayasym(alpha, meandelay);					// calculate the delayasymmetry
																		//
		fprintf(fp, "%lld\t%lld\t%lld\n", meandelay, delayasym, alpha);	// print values to a .txt file
		alpha += step;													//
	}																	//
	fclose(fp);
	return 0;
}
#endif
