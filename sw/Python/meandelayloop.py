import subprocess

from mpmath import *
mp.dps = 100

cmd = "fpa_python.c"
subprocess.call(["gcc",cmd]) #For Compiling

start = mpf(-4611686018427087903)		## start value of alpha
end = mpf(4611686018427087904)			## end value of alpha
maxmeandelay = mpf(mpf(40000)*mpf(5000)*mpf(2**15))		## average of 6,5 minutes with +/- (265 + 140) = 405 MB per file
filenumber = 1								## number of files with data
									## 80 for 400000 meandelaysteps and 1000 alpha steps = 80 files 32.4 GB and a total of 400 million lines takes +/- 8,5 hours
stepnum = 500								## adjust to make the files bigger and increase the step size of meandelay
step = int(mpf( mpf(maxmeandelay)/ mpf(mpf(filenumber)*mpf(stepnum)) ))	## calculation of the stepsize for meandelay

alphastep = 1000							## number of steps for looping through the range of alpha
alphastepsize = int(mpf(mpf(mpf(end) - mpf(start))/mpf(alphastep)))	## calculation of the stepsize for alpha
#alphastepsize = mpf(9223372036854776) ## 1000 steps

print(filenumber)

for j in range (0, filenumber):		## generation of file names
	str0 = "delayasym"		##
	str1 = "delayasymerr"		##
	str2 = str(j)			##
	str3 = ".txt"			##
	filename = str0+str2+str3	## result file
	errfile = str1+str2+str3	## error file

	reset_file = open(filename, 'w')## empty result file
	reset_file.write("")		##
	reset_file.close()		##

	for i in range (0, stepnum):											## loop for meandelay
		meandelay = mpf(mpf(maxmeandelay) - mpf(mpf(step)*mpf(i)))						##
		subprocess.call(["./a.out", filename, str(start), str(end), str(alphastepsize), str(meandelay)])	## compile C code with arguments
	print("donec", j)
	maxmeandelay = meandelay

	write_file = open(errfile, 'w')		## open error file

	if(write_file == 0):
		print("cant open filep \n")

	with open(filename, 'r') as f:											## loop through every line of the result file
		for line in f:												##
			meandelay, delayasym, alpha = line.split("\t")							## allocate the values in the file
			meandelay = int(mpf(meandelay))									##
			delayasym = int(mpf(delayasym))									##
			alpha = int(mpf(alpha))										##
															##
			real = mpf(mpf(mpf(meandelay) * mpf(alpha)) / mpf( mpf(alpha)+mpf(2**63) ))			## calculate the delayasymmetry with 100 decimal digit precision
			delayasymerr = int(mpf(mpf(real) - mpf(delayasym)))						## calculate the error
			alpha = mpf(mpf(alpha)/mpf(2**62))										##
															
			write_file.write("%s\t" % alpha)								## fill error file
			write_file.write("%s\t" % meandelay)								##
			write_file.write("%s\n" % delayasymerr)								##
